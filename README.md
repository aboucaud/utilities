`utilities`
===========

Set of convenient scripts that perform simple daily tasks
related to astronomical image processing.

#### Description

Below is the list of current scripts along with their quick description:
  - `mk_ellipticity.py`: compute ellipticity and quadrupole moments of an image (WL def.)
  - `check_pixelscale.py`: retrieve pixel scale information from the FITS header
  - `add_pixelscale.py`: add pixel scale information to the FITS header

#### Installation

There are two ways to get these scripts running:

  1. If you have `git` installed, the best way is to create a new directory and run the following command to retrieve the current version of the file,
    ```bash
    git clone git@git.ias.u-psud.fr:aboucaud/utilities.git
    ```

  2. Otherwise you will find on top of the [main page](https://git.ias.u-psud.fr/aboucaud/utilities) a download button to get a compressed archive containing the code.

Most of them only need the basic scientific Python libraries installed: **numpy**, **scipy**, **matplotlib** and **astropy** (or **pyfits**). Exceptions are listed below.

##### Exceptions
  -

If you have trouble installing these libraries, either contact the server administrator, or refer to [these instructions](https://git.ias.u-psud.fr/abeelen/python-notebook/blob/master/PythonInstall.md) for your personal computer.

Last, in order to run this code in any directory, you should add its location to your `PATH`.

#### Usage

For basic usage of these scripts, please refer to their own help message
```bash
my_script --help
```

#### Author
  Alexandre Boucaud <alexandre.boucaud@ias.u-psud.fr>

#### Version
  0.1
