#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function, division

try:
    import astropy.io.fits as pyfits
except ImportError:
    import pyfits


def main():
    import argparse
    parser = argparse.ArgumentParser(
        description="Add a pixel scale entry to the header of a FITS file")
    parser.add_argument(
        'filename',
        nargs='?',
        metavar='filename',
        type=str,
        help="filename of the FITS file")
    parser.add_argument(
        'pixel_scale',
        nargs='?',
        metavar='pixel_scale',
        type=float,
        help="pixel scale in arcseconds")
    args = parser.parse_args()

    # Load data and header from file
    data, header = pyfits.getdata(args.filename, header=True)

    # Make sure the file has not already that info
    pkey_list = ['PIXSCALE', 'PIXSCALX', 'SECPIX',
                 'CDELT1', 'CDELT2', 'CD1_1']
    pixel_key = None
    for key in pkey_list:
        if key in header.keys():
            pixel_key = key
            break
    if pixel_key:
        # pass
        raise ValueError("The pixel scale is already given in the file header")

    # Write the pixel scale
    header['CD1_1'] = args.pixel_scale / 3600
    header['CD1_2'] = 0
    header['CD2_1'] = 0
    header['CD2_2'] = args.pixel_scale / 3600

    # Save back changes
    pyfits.update(args.filename, data, header)

if __name__ == '__main__':
    main()
