#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
=================
mk_ellipticity.py
=================

Compute ellipticity information on the given image from the weighted
second moments, using an iterative scheme to find the image centroid

Usage:
  mk_ellipticity.py <fitsfile>
                    [-s, --sigma] [-p, --pix] [-n]
                    [-h, --help]

Args:
  fitsfile              input image in FITS format

Optionals:
  -h, --help            show this help message and exit
  -p, --pix             pixel scale [arcsec] of the input image
                        (default 1.0 arcsec/pix)
  -s, --sigma           width of the weighting gaussian distribution in arcsec
                        (default 0.75 arcsec)
  -n                    number of weighting iterations for the centroid
                        (default 4)

Example:
  mk_ellipticity.py psf.fits -p 0.01 -n 3

Author:
  Alexandre Boucaud <alexandre.boucaud@ias.u-psud.fr>

Version:
  1.0

Revision:
  June 10 2015
"""
from __future__ import print_function, division

import numpy as np
try:
    import astropy.io.fits as pyfits
except ImportError:
    import pyfits


def circ_gauss2d(shape, xc, yc, sigma):
    """
    Circular 2d Gaussian function

    Parameters
    ----------
    shape: tuple of floats
        Shape of the output array
    xc: float
        Center along the x-axis (column)
    yc: float
        Center along the y-axis (row)
    sigma: float
        Width of the Gaussian profile

    Returns
    -------
    output: `numpy.ndarray`
        Image of a circurlar 2d Gaussian function

    """
    y, x = np.indices(shape)
    exponent = 0.5 * ((x - xc)**2 + (y - yc)**2) / sigma**2
    return np.exp(-exponent) / (2 * sigma**2)


def first_moments(input):
    """
    Compute the first moments (axis means) of the given image

    Parameters
    ----------
    input: array_like
        Input image

    Returns
    -------
    mu_x: float
        First moment on the x-axis
    mu_y: float
        First moment on the y-axis

    """
    image = np.asarray(input, dtype=float)

    # Careful of Numpy image axes indexing
    y, x = np.indices(image.shape)
    total = image.sum()
    # First moments (centroid)
    mu_x = np.sum(x * image) / total
    mu_y = np.sum(y * image) / total

    return mu_x, mu_y


def second_moments(input):
    """
    Compute the second moments of the given image

    Parameters
    ----------
    input: array_like
        Input image

    Returns
    -------
    (q_xx, q_yy, q_xy): tuple of floats
        Second moments of the image

    """
    image = np.asarray(input, dtype=float)

    # Careful of Numpy image axes indexing
    y, x = np.indices(image.shape)
    total = image.sum()
    # First moments (centroid)
    mu_x, mu_y = first_moments(image)
    # Quadrupole moments
    q_xx = np.sum((x - mu_x) * (x - mu_x) * image) / total
    q_yy = np.sum((y - mu_y) * (y - mu_y) * image) / total
    q_xy = np.sum((x - mu_x) * (y - mu_y) * image) / total

    return q_xx, q_yy, q_xy


def centroid(input, sigma, pixscl, n_iter=3):
    """
    Compute the image centroid iteratively with Gaussian weights.

    The weight is a circular two-dimensional Gaussian function centered
    on the first moments of the image at each iteration if the center is
    not specified as input.

    Parameters
    ----------
    input: array_like
        Input image
    sigma: float
        Width of the weighting Gaussian in arcsec
    pixscl: float
        Pixel scale of the image to scale the Gaussian function
    n_iter: int, optional
        Number of iterations of the weighting (default 3)

    Returns
    -------
    mu_x, mu_y
        Coordinates of the cendroid in the image plane

    """
    image = np.asarray(input, dtype=float)

    # Scale the Gaussian width to the image pixel scale
    true_sigma = sigma / pixscl

    if n_iter <= 0:
        return first_moments(image)

    mu_x, mu_y = first_moments(image)
    for _ in range(n_iter):
        gweight = circ_gauss2d(image.shape, mu_x, mu_y, true_sigma)
        imweighted = image.copy() * gweight
        mu_x, mu_y = first_moments(imweighted)

    return mu_x, mu_y


def get_ellipticity(input, sigma=0.75, pixscl=0.1, n_iter=3):
    """
    Compute iteratively the weighted second moments of the image.

    The weight is a circular two-dimensional Gaussian function centered
    on the first moments of the image at each iteration if the center is
    not specified as input.

    Parameters
    ----------
    input: array_like
        Input image
    sigma: float, optional
        Width of the weighting Gaussian in arcsec (default 0.75)
    pixscl: float, optional
        Pixel scale of the image (default 0.1)
    n_iter: int, optional
        Number of iterations of the weighting (default 3)

    Returns
    -------
    res: dict
        Dictionary with first and second moments plus ellipticity

    """
    image = np.asarray(input, dtype=float)

    if n_iter <= 0:
        return second_moments(image)

    # First moments
    mu_x, mu_y = centroid(image, sigma, pixscl, n_iter=n_iter)

    gweight = circ_gauss2d(image.shape, mu_x, mu_y, sigma / pixscl)
    imweighted = image.copy() * gweight

    # Second moments
    q_xx, q_yy, q_xy = second_moments(imweighted)

    # Ellipticity components
    e1 = (q_xx - q_yy) / (q_xx + q_yy)
    e2 = 2 * q_xy / (q_xx + q_yy)

    r2 = (q_xx + q_yy) * pixscl ** 2

    res = {
        'mu_x': mu_x,
        'mu_y': mu_y,
        'q_xx': q_xx,
        'q_yy': q_yy,
        'q_xy': q_xy,
        'e1': e1,
        'e2': e2,
        'r2': r2
    }

    return res


def parse_args():
    """Command-line parser"""
    import argparse

    parser = argparse.ArgumentParser(
        description=(
            "Compute ellipticity information on the given image "
            "from the weighted second moments, using an iterative "
            "scheme to find the image centroid")
        )

    parser.add_argument(
        'fitsfile',
        nargs='?',
        metavar='fitsfile',
        type=str,
        help="input image in FITS format")

    parser.add_argument(
        '-s',
        '--sigma',
        nargs='?',
        metavar='sigma',
        type=float,
        default=0.75,
        const=0.75,
        dest='sigma',
        help=("width of the weighting gaussian distribution "
              "in arcsec (default 0.75)"))

    parser.add_argument(
        '-p',
        '--pix',
        nargs='?',
        metavar='pixel_scale',
        type=float,
        default=1.0,
        const=1.0,
        dest='pixel_scale',
        help=("pixel scale [arcsec] of the input image "
              "(default 1.0 arcsec/pix)"))

    parser.add_argument(
        '-n',
        nargs='?',
        metavar='n_iter',
        type=int,
        default=4,
        const=4,
        dest='n_iter',
        help="number of weighting iterations for the centroid (default 4)")

    return parser.parse_args()


def main():
    args = parse_args()

    image = pyfits.getdata(args.fitsfile)

    res = get_ellipticity(image,
                          sigma=args.sigma,
                          pixscl=args.pixel_scale,
                          n_iter=args.n_iter)

    text = (
        "\n"
        "Ellipticity measurement on {fil} done in {n_iter} iterations "
        "using a gaussian weight of {sigma} arcsec\n"
        "---\n"
        "mu_x = {mu_x:1.4f}, mu_y = {mu_y:1.4f}\n"
        "q_xx = {q_xx:1.4f}, q_yy = {q_yy:1.4f}, q_xy = {q_xy:1.4f}\n"
        "e1 = {e1:1.6f}, e2 = {e2:1.6f}, r2 = {r2:1.6f}")
    print(text.format(fil=args.fitsfile, n_iter=args.n_iter,
                      sigma=args.sigma, **res))


if __name__ == '__main__':
    main()
    # try:
    #     main()
    # except:
    #     print("A problem occured, check the following documentation")
    #     print(__doc__)
