#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
================
check_pixelscale
================
Retreive the pixel scale info from FITS files

If called without input files or pattern, defaults to all FITS files
in current directory

Usage:
  check_pixelscale [--pattern] <input>
                   [--help]

Args:
  input           file list or pattern

Optionals:
  --help          print help (this)
  --pattern       use a glob pattern instead of a list of files

Examples:
  check_pixelscale image_a.fits image_b.fits
  check_pixelscale --pattern image*.fits

Author:
  Alexandre Boucaud <alexandre.boucaud@ias.u-psud.fr>

Version:
  0.1
"""
from __future__ import print_function

try:
    import astropy.io.fits as pyfits
except ImportError:
    import pyfits
from os import path


def get_pixscale(filename):
    """
    Retreive the image pixel scale in its FITS header

    Parameters
    ----------
    filename: str
        Path to the FITS file

    Returns
    -------
    pixel_scale: float
        Pixel scale of the image in arcseconds

    """
    header = pyfits.getheader(filename)
    pixel_key = None
    pkey_list = ['PIXSCALE', 'PIXSCALX', 'SECPIX',
                 'CDELT1', 'CDELT2', 'CD1_1']
    for key in pkey_list:
        if key in header:
            pixel_key = key
            break
    if not pixel_key:
        raise IOError("Pixel size not found in FITS file")

    pixel_scale = abs(header[pixel_key])
    if pixel_key in ['CDELT1', 'CDELT2', 'CD1_1']:
        pixel_scale *= 3600

    return pixel_scale


def print_usage_and_exit():
    print(__doc__)
    import sys
    sys.exit(0)


def parse_args(argv):
    from glob import glob

    if '--help' in argv:
        print_usage_and_exit()

    # Read input from calling sequence, if any
    dirlist = argv[1:]

    # If no elements in the list, defaults to all FITS files in dir
    if not dirlist:
        dirlist = glob('*.fits')
    # or if pattern keyword set, then read following input as pattern
    elif dirlist[0] == '--pattern':
        if len(dirlist) == 2:
            dirlist = glob(dirlist[1])
        else:
            print_usage_and_exit()

    return dirlist


def main(argv):
    dirlist = parse_args(argv)

    print("Name of the file", "Pixel scale in arcseconds", sep=" | ")
    print("---")
    badfilelist = []
    for psfile in dirlist:
        try:
            pixel_scale = get_pixscale(psfile)
            print(path.basename(psfile), pixel_scale, sep=" | ")
        except IOError:
            badfilelist.append(psfile)

    if badfilelist:
        print("*** Issue with following file(s):", end='\n')
        print(*badfilelist, sep='\n')
    else:
        print("No bad file found in this directory")

if __name__ == '__main__':
    import sys
    main(sys.argv)
